import java.util.*;
public class ConstDest1{
    String fullName;
    int rollNum;
    double semPercentage;
    String collegeName;
    int collegeCode;
    ConstDest1(String name,String collName,int rNum,int collCode,double spercentage){
        fullName=name;
        rollNum=rNum;
        semPercentage=spercentage;
        collegeName=collName;
        collegeCode=collCode;
    }
    void display()
    {
        System.out.println("Full Name:"+fullName);
        System.out.println("Roll Number:"+rollNum);
        System.out.println("Sem Percentage:"+semPercentage);
        System.out.println("college name:"+collegeName);
        System.out.println("College Code:"+collegeCode);
    }
    protected void finalize() throws Throwable
    {
       System.out.println("Object destroyed");
    } 
    
public static void main(String[]args)
{
    System.out.println("Student Details:");
    Scanner input = new Scanner(System.in);
    System.out.println("Enter college Name: ");
    String collName = input.nextLine();
    System.out.println("Enter name: ");
    String name= input.nextLine();
    System.out.println("Enter roll number: ");
    int rNum = input.nextInt();
    System.out.println("Enter sem percentage: ");
    double spercentage = input.nextDouble();
    System.out.println("Enter college code: ");
    int collCode = input.nextInt();
    ConstDest1 obj = new ConstDest1(name,collName,rNum,collCode,spercentage);
    obj.display();
}
}

