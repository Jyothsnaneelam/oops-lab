import java.util.*;
public class ConstDestJava
{
  String name;
    int rNum;
    double percentage;
    String collName;
    int collCode;
  ConstDestJava(String fullName,String collegeName, int rollNum, int collegeCode, double semPercentage) //constructor
  {
    name = fullName;
    collName = collegeName;
    rNum = rollNum;
    collCode = collegeCode;
    percentage = semPercentage;
  }
  void display()
   {
    System.out.println("College Name : "+ collName);
    System.out.println("College code : "+ collCode);
    System.out.println("Roll Number : "+ rNum);
    System.out.println("Full Name : "+ name);
    System.out.println("Sem Percentage : "+ percentage);
  }
  protected void finalize() throws Throwable{ System.out.println(" DEAD ");} 
  public static void main(String[] args)
  {
    System.out.println("Student Details:");
    Scanner input = new Scanner(System.in);
    System.out.println("Enter college Name: ");
    String collegeName = input.nextLine();
    System.out.println("Enter name: ");
    String fullName = input.nextLine();
    System.out.println("Enter roll number: ");
    int rollNum = input.nextInt();
    System.out.println("Enter sem percentage: ");
    double semPercentage = input.nextDouble();
    System.out.println("Enter college code: ");
    int collegeCode = input.nextInt();
    ConstDestJava obj = new ConstDestJava(fullName,collegeName,rollNum,collegeCode,semPercentage);
    obj.display();
    obj=null;
    System.gc(); 
  }    
}
