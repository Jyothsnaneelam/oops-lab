import java.util.*;
interface Shape{
    void display(int a, int b);
}
class Area implements Shape{
    public void display(int a,int b){
        System.out.println("The Area is "+a*b);
    }
}
class Perimeter implements Shape{
    public void display(int a,int b){
        System.out.println("The Area is "+a+b);
    }
}

public class PureAbs{
    public static void main(String[] args){
        Shape area = new Area();
        Shape peri = new Perimeter();
        area.display(3, 4);
        peri.display(3, 4);
    }
}


