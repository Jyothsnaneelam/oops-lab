  class Parent{
    void start(){
        System.out.println("The vehicle has started");
    }
}

class Child extends Parent{
    void start(){
        System.out.println("The vehicle is moving");
    }
}

class Vehicle{
   
    public static void main(String[] args) {
        Child V = new Child();
        V.start();    
    }
}

