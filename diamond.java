class A {
   public void methodA() {
      System.out.println("Method A");
   }
}

class B extends A {
   public void methodB() {
      System.out.println("Method B");
   }
}

class C extends A {
   public void methodC() {
      System.out.println("Method C");
   }
}
class Diamond{
class D extends B, C {
   public void methodD() {
      super.methodA(); // explicitly call methodA() from class B
      System.out.println("Method D");
   }
}
}

