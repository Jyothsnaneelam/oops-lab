import java.util.Scanner;

class EvenOddjava
{
    public static void main(String args[])
    {
        System.out.println("Enter an integer :");
        Scanner input=new Scanner(System.in);
        int n=input.nextInt();
        func(n);
    }
    public static void func(int n)
    {
        if(n%2==0)
        {
           System.out.println("The number "+n+" is even."); 
        }
        else
        {
            System.out.println("The number "+ n +" is odd.");
        }
    }
}