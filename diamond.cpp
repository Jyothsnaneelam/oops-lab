#include <iostream>
using namespace std;

class A {
public:
   virtual void display() {
      cout << "Displaying A\n";
   }
};

class B: virtual public A {
public:
   virtual void display() {
      cout << "Displaying B\n";
   }
};

class C: virtual public A {
public:
   virtual void display() {
      cout << "Displaying C\n";
   }
};

class D: public B, public C {
public:
   virtual void display() {
      cout << "Displaying D\n";
   }
};

int main() {
   D d;
   d.display();
   return 0;
}
