class Inheritance{
	public int pub=8;
	protected int  prot=16;
	private int priv=;
	public int callpri(){
    	    return priv;
	}
}

class Child extends Inheritance{
	int callpro(){
    	   return prot;
	}
}
public class PPPInheriJava{
	public static void main(String[] args){
     	Child obj= new Child();
     	System.out.println("Value in public : "+obj.pub);
     	System.out.println("Value in protected : "+obj.callpro());
     	System.out.println("Value in private : "+obj.callpri());
	}
}
