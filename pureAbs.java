import  java.util.*;
interface Shape{
    void display(int a, int b);
}
class Area implements Shape{
    public void display(int a,int b){
        System.out.println("The Area is "+a*b);
    }
}
class Perimeter implements Shape{
    public void display(int a,int b){
        System.out.println("The Area is "+a+b);
    }
}

public class pureAbs{
    public static void main(String[] args){
        Shape area = new Area();
        Shape peri = new Perimeter();
        area.display(7, 8);
        peri.display(9, 4);
    }
}

