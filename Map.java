import java.util.*;
public class MapExample {
public static void main(String[] args) {
Map<String, Integer> studentScores = new HashMap<>();
studentScores.put("Akhil", 90);
studentScores.put("Bobby", 85);
studentScores.put("Charan", 95);
int score1 = studentScores.get("Akhil");
int score2 = studentScores.get("Bobby");
System.out.println("Akhil's score: " + score1);
System.out.println("Bobby's score: " + score2);
boolean containsKey = studentScores.containsKey("Charan");
System.out.println("Does map contain Charlie? " + containsKey);
studentScores.put("Akhil", 92);
studentScores.remove("Bobby");
System.out.println("Keys in the map:");
for (String key : studentScores.keySet()) {
System.out.println(key);
}
System.out.println("Values in the map:");
for (int value : studentScores.values()) {
System.out.println(value);
}
System.out.println("Key-value pairs in the map:");
for (Map.Entry<String, Integer> entry : studentScores.entrySet()) {
String name = entry.getKey();
int score = entry.getValue();
System.out.println(name + ": " + score);
}
}
}
