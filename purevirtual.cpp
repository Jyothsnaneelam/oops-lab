#include<iostream>
using namespace std;

class Square {  
    public:
    virtual int Answer(int Side) = 0;

};

class Area : public Square {
public:
    int Answer(int Side) {
        return Side*4;
    }
};

class Perimeter : public Square {
public:
    int Answer(int Side) {
        return Side*Side;
    }
};

int main() {
    Area area;
    Perimeter peri;
    int Side;
    cout<<"Enter the dimension of the square"<<endl;
    cin>>Side;
    cout<<"The Area of the square is "<<area.Answer(Side)<<endl;
    cout<<"The Perimeter of the square is "<<peri.Answer(Side)<<endl;

}

