 import java.util.Scanner;

public class user_def {
  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);

    try {
      System.out.print("Enter your age: ");
      int age = scanner.nextInt();
      if (age < 18) {
        throw new InvalidAgeException("Age must be greater than or equal to 18!");
      } else {
        System.out.println("Welcome to the party!");
      }
    } catch (InvalidAgeException e) {
      System.out.println("Exception caught: " + e);
    } catch (Exception e) {
      System.out.println("Something went wrong: " + e);
    }
  }
}

class InvalidAgeException extends Exception {
  public InvalidAgeException(String message) {
    super(message);
  }
}
