#include<iostream>
using namespace std;
class Ab
{
  public:
  void displayAb()
  {
      cout<<"Welcome Ab"<<endl;
  }
};
class Abc
{
  public:
  void displayAbc()
  {
      cout<<"Welcome Abc"<<endl;
  }
};
//Simple Inheritance
class chapter1 : public Ab
{
    public:
    void displayCh1()
    {
        cout<<"Hi Chapter1"<<endl;
    }
};
//Multiple Inheritance
class chapter2 : public Ab
{
    public:
    void displayCh2()
    {
        cout<<"Hi Chapter2"<<endl;
    }
};
class chapter3 : public chapter1
{
    public:
    void displayCh3()
    {
        cout<<"Hi Chapter3"<<endl;
    }
};
class chapter4 : public Abc
{
    public:
    void displayCh4()
    {
        cout<<"Hi Chapter4"<<endl;
    }
};
class chapter5 : public Abc
{
    public:
    void displayCh5()
    {
        cout<<"Hi Chapter5"<<endl;
    }
};
int main()
{
    chapter1 c1;
    chapter2 c2;
    chapter3 c3;
    chapter4 c4;
    chapter5 c5;
    cout<<"Simple Inheritance"<<endl;
    c1.displayAb();
    cout<<"Multiple Inheritance"<<endl;
    c2.displayAb();
    c2.displayCh2();
    cout<<"Multilevel Inheritance"<<endl;
    c3.displayCh1();
    c3.displayCh3();
    cout<<"Heirarchial Inheritance"<<endl;
    c4.displayCh4();
    c4.displayAbc();
    c5.displayCh5();
    c5.displayAbc();
    return 0;
}
