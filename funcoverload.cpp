#include <iostream>    
using namespace std;    
class Cal {    
    public:    
     int add(int a,int b){      
        return a + b;      
    }      
     int add(int a, int b, int c)      
    {      
        return a + b + c;      
    }      
};     
int main(void) {    
    Cal C;                                                    //     class object declaration.   
    cout<<C.add(1, 20)<<endl;      
    cout<<C.add(2, 20, 3)<<endl;     
   return 0;    
}    
