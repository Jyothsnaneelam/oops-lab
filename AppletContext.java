import java.applet.Applet;
import java.applet.AppletContext;
import java.net.*;
/*
<applet code=”LoadHTMLFileSample” width=”700″ height=”500″></applet>
*/
public class LoadHTMLFileSample extends Applet
{
    public void start()
    {
     AppletContext context= getAppletContext();
     //get AppletContext
     URL codeBase = getCodeBase(); //get Applet code base
     try{
         URL url = new URL(codeBase + “Test.html”);
            context.showDocument(url,”_blank”);
            repaint();
         }catch(MalformedURLException mfe) {
                         mfe.printStackTrace();
                         }
     }
}
