 #include<iostream>
using namespace std;
class Inh{
	public:
   	int pub=8;
	protected:
   	int  prot=16;
	private:
    	int priv=21;
	public:
	int callpri(){
    	return priv;
	}
};

class Child : public Inh{
	public:
	int callpro(){
    	return prot;
	}
};
int main(){
Child obj;
	cout<<"Value in public : "<<obj.pub<<endl;
	cout<<"value in protected : "<<obj.callpro()<<endl;
	cout<<"value in private : "<<obj.callpri()<<endl;
}


