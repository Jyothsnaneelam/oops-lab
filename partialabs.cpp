#include<iostream>
using namespace std;
class Impure{
    public:
    virtual void student()=0;
    void parent(){
        cout<<"hello"<<endl;
    }
};
class Child:public Impure{
    public:
    void student(){
        cout<<"Need to study."<<endl;
    }
};
int main()
{
    Child obj;
    obj.parent();
    obj.student();
}