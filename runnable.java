class MyRunnable implements Runnable {
    @Override
    public void run() {
        System.out.println("Thread running...");
    }
}

public class Runnable_inter {
    public static void main(String[] args) {
        MyRunnable myRunnable = new MyRunnable();
        Thread thread = new Thread(myRunnable);
        thread.start();
    }
}

